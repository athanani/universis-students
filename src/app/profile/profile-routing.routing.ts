import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FullLayoutComponent} from '../layouts/full-layout.component';
import {AuthGuard} from '@universis/common';
import {ProfileHomeComponent} from './components/profile-home/profile-home.component';

const routes: Routes = [
    {
        path: '',
        component: ProfileHomeComponent,
        canActivate: [
            AuthGuard
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRoutingModule {
}
