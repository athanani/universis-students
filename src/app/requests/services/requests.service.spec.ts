import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import { TranslateModule } from '@ngx-translate/core';

import { RequestsService } from './requests.service';
import {TestingConfigurationService} from '../../test';

describe('RequestsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
          {
              provide: ConfigurationService,
              useClass: TestingConfigurationService
          },
        RequestsService
      ],
      imports: [ HttpClientTestingModule,
          TranslateModule.forRoot(),
          MostModule.forRoot({
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          })
      ]
    });
  });

  it('should be created', inject([RequestsService], (service: RequestsService) => {
    expect(service).toBeTruthy();
  }));
});
