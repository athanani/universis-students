import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {CourseGradeBase, GradeAverageResult, GradesService} from '../../services/grades.service';
import {ConfigurationService, ErrorService, GradeScale, GradeScaleService, ModalService} from '@universis/common';
import { LoadingService } from '@universis/common';
import { ChartOptions } from 'chart.js';

@Component({
  selector: 'app-grades-recent',
  templateUrl: './grades-recent.component.html',
  styleUrls: ['./grades-recent.component.scss']
})
export class GradesRecentComponent implements OnInit {

  public latestCourses: any = [];
  public isCollapsed: boolean[];
  public isLoading = true;


  /* for stats box */
  public simpleAverageObj: GradeAverageResult = {
    courses: 0,
    coefficients: 0,
    average: 0,
    ects: 0,
    units: 0,
    passed: 0,
    grades: 0,
  };
  public defaultGradeScale: GradeScale;
  private courseTeachers: any;
  private latestExamPeriod: string;

  /* for stats box */
  public registeredCourseCount: number;
  public passedCourseCount: number;
  public failedCourseCount: number;
  public passedGradeAverage: string;

  /* modal values */
  @ViewChild('modalTemplate') modalTemplate: TemplateRef<any>;
  private modalRef: any;
  private modalCourse: any;
  public studentsRegisters = 0;
  public studentsGraded = 0;
  public studentsSuccessed = 0;
  // Doughnut
  public doughnutChartLabels;
  public doughnutChartData;
  public doughnutChartType = 'doughnut';
  public doughnutOptions: ChartOptions = {
    responsive: true,
    legend: {
      display: true,
      position: 'bottom'
    }
  };

  public defaultLanguage = null;
  public currentLanguage = null;

  public pendingGrades: any[];

  constructor(private gradesService: GradesService,
              private loadingService: LoadingService,
              private modal: ModalService,
              private gradeScaleService: GradeScaleService,
              private  errorService: ErrorService,
              private _configurationService: ConfigurationService) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService.settings.i18n.defaultLocale;
  }

  ngOnInit() {
    // show loading
    this.loadingService.showLoading();

    this.gradesService.getRecentGrades().then((res) => {
      // get default grade scale
      return this.gradesService.getDefaultGradeScale().then(gradeScale => {
        // set grade scale
        this.defaultGradeScale = gradeScale;
        this.latestCourses = res;
        this.latestCourses.sort((a, b) => a.course.name.localeCompare(b.course.name));
        this.isCollapsed = Array(this.latestCourses.length).fill(true);
        const gradeGroup: Array<CourseGradeBase> = [];

        this.latestCourses.forEach(course => {
          const courseGradeBaseObj: CourseGradeBase = {
            courseStructureType: 1,
            units: course.course.units,
            calculateGrade:  course.course && course.course.gradeScale &&  course.course.gradeScale.scaleType !== 3,
            calculateUnits: true,
            isPassed: course.isPassed,
            ects: course.course.ects,
            coefficient: 1,
            grade: course.grade1
          };
          gradeGroup.push(courseGradeBaseObj);
        });

        // set average of passed courses
        this.simpleAverageObj = this.gradesService.getGradesSimpleAverage(gradeGroup);
        if (this.simpleAverageObj) {
          // set passed courses
          this.passedCourseCount = this.simpleAverageObj.passed;
          // set failed courses
          this.failedCourseCount = this.simpleAverageObj.courses - this.simpleAverageObj.passed;
          // set sum of courses
          this.registeredCourseCount = this.simpleAverageObj.courses;
          // set Simple average of passed courses
          this.passedGradeAverage = this.defaultGradeScale.format(this.simpleAverageObj.average);
        }
        this.gradesService.getPendingGrades().then((pendingGrades) => {
          this.pendingGrades = pendingGrades;
          // hide loading
          this.loadingService.hideLoading();
          this.isLoading = false;
        }).catch(err => {
          // TODO: handle error
          console.error(err);
        });
      }).catch(err => {
        // TODO: handle error
        console.error(err);
      });
    }).catch(err => {
      // TODO: handle error
      console.error(err);
    });
  }

  showStatistics(course) {
    this.modalCourse = course;
    this.loadStatistics();
  }

  closeModal() {
    this.modalRef.hide();
  }

  loadStatistics() {
    this.isLoading = true;
    this.loadingService.showLoading();
    this.gradeScaleService.getGradeScale(this.modalCourse.course.gradeScale.id).then( gradeScale => {
      this.gradesService.getGradesStatistics(this.modalCourse.courseExam.id).then((result) => {
      const data = result;
        this.studentsRegisters = data
        // map count
          .map(x => x.total)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        // get students with grade
        this.studentsGraded = data
        // filter students by grade
          .filter(x => {
            return x.examGrade != null;
          })
          // map count
          .map(x => x.total)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        // get students passed
        this.studentsSuccessed = data
        // filter passed grades
          .filter(x => {
            return x.isPassed;
          })
          // map count
          .map(x => x.total)
          // calculate sum
          .reduce( (a, b) => a + b, 0);
        // get grade scale values
        let gradeScaleValues = [];
        // numeric grade scale
        if (gradeScale.scaleType === 0) {
          // get grade scale base
          const gradeScaleMinValue = parseInt(gradeScale.format(gradeScale.scaleBase), 10);
          // get grade scale max
          const gradeScaleMaxValue = parseInt(gradeScale.format(1), 10);

          for (let grade = gradeScaleMinValue; grade < gradeScaleMaxValue; grade++) {
            gradeScaleValues.push({
              valueFrom: gradeScale.convert(grade),
              valueTo: gradeScale.convert(grade + 1),
              name: `${grade}-${grade + 1}`,
              total: 0
            });
          }
        } else {
          gradeScaleValues = gradeScale['values'].map ( value => {
            return {
              valueFrom: value.valueFrom,
              valueTo: value.valueTo,
              name: value.name,
              total: 0
            };
          });
        }

        // get total students per value
        gradeScaleValues.forEach( value => {
          value.total = data.filter( x => (x.examGrade === 1 && x.examGrade >= value.valueFrom && x.examGrade <= value.valueTo) ||
            (x.examGrade >= value.valueFrom && x.examGrade < value.valueTo))
            .map(x => x.total)
            .reduce( (a, b) => a + b, 0);
        });

        // get chart data
        const _chartGradesData: any = gradeScaleValues.map( x => x.total );
        const _chartGradesLabels: any = gradeScaleValues.map( x => x.name);

        this.doughnutChartLabels = _chartGradesLabels;
        this.doughnutChartData = [ _chartGradesData ];

        this.modalRef = this.modal.openModal(this.modalTemplate, 'modal-lg');
        this.isLoading = false;
        this.loadingService.hideLoading();
      }).catch(err => {
        this.isLoading = false;
        this.loadingService.hideLoading();
        return this.errorService.navigateToError(err);
      });
    }).catch(err => {
      this.isLoading = false;
      this.loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    });

  }
}
