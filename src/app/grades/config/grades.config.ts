interface GradesConfiguration {
    groups: Array<GradesGroupConfiguration>;
    requiredFields: Array<GradesRequiredFields>
}

interface GradesGroupConfiguration {
    title: string;
    attribute: string;
    key: string;
    value: string;
}

interface GradesRequiredFields {
  title: string;
  attribute: string;
}
