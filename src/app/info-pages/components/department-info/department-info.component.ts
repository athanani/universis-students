import { Component, OnInit } from '@angular/core';
import { LoadingService } from '@universis/common';
import {ProfileService} from '../../../profile/services/profile.service';

@Component({
  selector: 'app-department-info',
  templateUrl: './department-info.component.html',
  styleUrls: ['../../../profile/components/profile-preview/profile-preview.component.scss']
})
export class DepartmentInfoComponent implements OnInit {

  public department: any;
  public loading = true;

  constructor(private _profileService: ProfileService,
              private loadingService: LoadingService) {}

  async ngOnInit() {
    this.loadingService.showLoading();
    const student = await this._profileService.getStudent();
    this.department = student.department;
    this.loading = false;
    this.loadingService.hideLoading();
  }

}
