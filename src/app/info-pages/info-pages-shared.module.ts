import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {environment} from '../../environments/environment';
import {SharedModule} from '@universis/common';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        MostModule
    ],
    declarations: [

    ],
    exports: [

    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InfoPagesSharedModule {

    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/info.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
